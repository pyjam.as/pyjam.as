How to deploy:

```bash
ssh pyjamas
sudo -s
cd /pyjamas/
docker compose pull
docker compose up -d
```
