from glob import glob

from PIL import Image


topping_paths = glob("/pyjamas/pizza/toppings/*.png")

toppings = {
    path.split("/")[-1].removesuffix(".png"): Image.open(path) for path in topping_paths
}
base = Image.open("/pyjamas/pizza/base.png")


def top(layer1, layer2):
    new = layer1.copy()

    for x in range(32):
        for y in range(32):
            new_pixel = layer2.getpixel((x, y))
            if new_pixel == (0, 0, 0, 0):
                continue
            new.putpixel((x, y), new_pixel)

    return new


def bake(*layers):
    pizza = base

    for layer in layers:
        pizza = top(pizza, toppings[layer])

    return pizza
