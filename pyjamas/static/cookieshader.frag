#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    // Normalized pixel coordinates (from -1 to 1)
    vec2 uv = (fragCoord - 0.5 * u_resolution.xy) / (min(u_resolution.x, u_resolution.y) * 0.5);

    const float rotate_speed = 0.2;
    const float rotate_amount = 0.8;
    float theta = sin(u_time * rotate_speed) * rotate_amount;
    uv = vec2(
        cos(theta) * uv.x - sin(theta) * uv.y,
        sin(theta) * uv.x + cos(theta) * uv.y
    );

    float stripewidth = 20. + 55.*abs(uv.x / 20.);
    const float stripespeed = 1.5;
    if (sin(uv.x * stripewidth + stripespeed * u_time) < 0.) {
        fragColor = vec4(1., 1., 1., 1.);
    } else {
        fragColor = vec4(.8, .8, 1., 1.);
    }
}

void main() {
    mainImage(gl_FragColor, gl_FragCoord.xy);
}
