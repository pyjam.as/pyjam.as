from quart import Blueprint, render_template, redirect
from quart import send_from_directory, send_file
from loguru import logger
import io

from . import pizza


bp = Blueprint(
    "hello",
    __name__,
    template_folder="templates",
    static_folder="static",
    static_url_path="/static",
)


@bp.route("/")
async def hello_world():
    logger.info("Serving Hello World page.")
    return await render_template("hello.j2.html")


@bp.route("/epoke")
async def epoke():
    logger.info("Serving epoke page.")
    return await render_template("epoke.j2.html")


@bp.route("/robots.txt")
async def robots():  # TODO:  🤖 as function name
    return await send_from_directory(bp.static_folder, "robots.txt")


@bp.route("/CTF{not_this_🌰}/")
async def robots2():
    return redirect("https://pyjam.as/", code=418)


@bp.route("/HA-GØH")
async def hagøh():
    logger.info("Serving HA-GØH page.")
    return await render_template("hag.j2.html")


@bp.route("/hackervandfald")
async def vandfald():
    logger.info("Serving hackervandfald page.")
    return await render_template("vandfald.j2.html")


@bp.route("/portal")
async def links():
    logger.info("Serving portal page.")
    return await render_template("portal.j2.html")


@bp.route("/writeups/fectf2022")
async def fectf2022():
    return await render_template("fectf2022.j2.html")


@bp.route("/pizzart/<path:path>")
async def pizzart(path):
    toppings = path.split("/")
    cursor = pizza.bake(*toppings)
    raw = io.BytesIO()
    cursor.save(raw, format="PNG")
    return await send_file(raw, mimetype="image/png")


@bp.route("/gamejam")
async def gamejam():
    return redirect("https://itch.io/jam/bornhack-2024-gamejam")


@bp.route("/skål")
async def skaal():
    return redirect("/static/skål.mp3")


@bp.route("/jooks")
@bp.route("/hvheh")
@bp.route("/hunvilhaenhacker")
@bp.route("/hunvilhaveenhacker")
@bp.route("/jegvilhaenhacker")
@bp.route("/jegvilhaveenhacker")
async def jegvilhaenhacker():
    logger.info("Serving Hun vil ha' en Hacker page.")
    return await render_template("jegvilhaenhacker.j2.html")


@bp.route("/tdc")
async def teglholmsgade1():
    logger.info("Serving Teglholmsgade_1.mp3")
    return await render_template("teglholmsgade_1.j2.html")
